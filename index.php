<!DOCTYPE html>
<html>
  <head>

  </head>

  	

  <body class="bg-dark">
    <div class="container">
	<div id="minbd">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login Panel</div>
        <div class="card-body">
          <form action="dbcon/db_signin.php" method="POST">
            <div class="form-group">
              <div class="form-label-group">
			  <div>email</div>
                <input type="email" id="email" name="email" class="form-control" placeholder="Please enter your Username." required="required" autofocus="autofocus">
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
			  	<div>Password</div>
                <input type="password" id="password" name="password" class="form-control" placeholder="Please enter a password." required="required">
              </div>
            </div>

			<input type="submit" class="btn btn-primary btn-block" value="LOGIN">
			<div class="text-center">
				<a class="d-block small mt-3" href="signup.php">Signup</a>
            </div>
          </form>
          
        </div>
      </div>
    </div>


  </body>

</html>
