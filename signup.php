﻿<!DOCTYPE html>
<html lang="en">

  <head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

  </head>
    		
  <body class="bg-dark">
    <div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header"><b>Register Panel</b></div>
        <div class="card-body">
          <form action="dbcon/db_signingup.php" method="POST">
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                    <input type="text" id="firstname" name="firstname" class="form-control" placeholder="Please enter First Name" required="required" autofocus="autofocus">
                </div>
              </div>
            </div>
			<div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                    <input type="text" id="lastname" name="lastname" class="form-control"  placeholder="Please enter Last Name" required="required" autofocus="autofocus">
                </div>
              </div>
            </div>
			<div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                    <input type="text" id="address" name="address" class="form-control"  placeholder="Please enter your Address" required="required" autofocus="autofocus">
                </div>
              </div>
            </div>
			<div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                    <input type="text" id="phonenum" name="phonenum" class="form-control"  placeholder="Please enter your Phone Number" required="required" >
                </div>
              </div>
            </div>			
            <div class="form-group">
                <input type="password" id="password" name="password" class="form-control" onKeyPress="return disableEnterKey(event)" placeholder="Please enter a password." required="required">
            </div>
			<div class="form-group">
                <input type="email" id="email" name="email" class="form-control" onKeyPress="return disableEnterKey(event)" placeholder="Please enter email address" required="required">
            </div>
            <input type="submit" class="btn btn-primary btn-block" value="Sign up">
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="index.php">Login</a>
          </div>
        </div>
      </div>
    </div>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  </body>

</html>
