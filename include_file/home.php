<!DOCTYPE html>
<html lang="en">
  <head>
    <style>
    h5 { color: #black; }
  </style>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="js/default.css?ver=171222">
	<link rel="stylesheet" href="js/style.css" type="text/css">


	
</head>
<body>
<body>
<div id="layout">
	<div class="header">
		<div class="logo">
			<a href="home.php">
		</div>
		<div class="menu">
			<div class="gnb">
				<div class="gnb_box">
					<ul class="depth1">
					    <li><a class="nav1st" href="home.php">Home</a></li>														
                    </ul>
			</div>
		</div>
		</div>
		</div>
		</div>

		<br>
		<br>
		<br>
			<div class="row">
				<div class="col-md-1">&nbsp;</div>

				<div class="col-md-10">
					<div class="card-body card text-white bg-dark o-hidden h-100">
          


		<br/>


	
			  	<!--######################################################################################-->	  

<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" >Update Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <form action="dbcon/db_update.php" method="POST">
			<div class="modal-body">
				<input type="hidden" name="usersid" id="usersid">
				<div class="form-group">
					<label> First Name</label>
					<input type="text" name="firstname" id="firstname" class="form-control" >
				</div>
				<div class="form-group">
					<label> Last Name</label>
					<input type="text" name="lastname" id="lastname" class="form-control">
				</div>
				<input type="hidden" name="dateregistered" id="dateregistered">
				<div class="form-group">
					<label> Address</label>
					<input type="text" name="address" id="address" class="form-control">
				</div>
				<div class="form-group">
					<label> Phone Number</label>
					<input type="text" name="phonenum" id="phonenum" class="form-control">
				</div>
				<div class="form-group">
					<label> Email Address</label>
					<input type="text" name="email" id="email" class="form-control">
				</div>		
			</div>	    
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="updatedata" class="btn btn-primary">Save changes</button>
      </div>
	  </form>
    </div>
  </div> 
</div>
	<!--######################################################################################-->	  
	
	<!--######################################################################################-->	  

<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" >Delete Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <form action="dbcon/db_delupdate.php" method="POST">
			<div class="modal-body">
				<input type="hidden" name="usersid" id="usersid">
				
					<label> Are you sure you want to delete the data?</label>
					
			</div>	    
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="deletedata" class="btn btn-primary">Proceed</button>
      </div>
	  </form>
    </div>
  </div> 
</div>
	<!--######################################################################################-->	 
		  
	<div id="users-contain" class="row">
		<div class="col-md-1">&nbsp;</div>
		<div class="col-md-8">
		<div class="card-body text-white bg-dark o-hidden h-100">	
		<table class="table table-bordered table-striped" id="editable_table" >	
		<div class="container">
		<div class="row">
		</div>
				  </span>
				</a>
			  </div>
			</div>
		</div>
	
		
		  <thead>	 
		  <tr>
		  <th>Users ID</th>
		    <th>First Name</th>
			<th>Last Name</th>
		    <th>Date Registered</th>
			<th>Address</th>
			<th>User Phone</th>
			<th>Email</th>
			<th>Edit</th>
			<th>Delete</th>
			

		
		  </tr>
		  </thead>
<tbody>
		<?php 
		  if($cnt==0){}else{
			  

		  for($i=0;$i<sizeof($usersID1);$i++){ ?>
		  <tr>
		   <td><?php echo $usersid[$i]?></td>
			<td><?php echo $firstname[$i]?></td>
			<td><?php echo $lastname[$i]?></td>
			<td><?php echo $dateregistered[$i]?></td>
			<td><?php echo $useraddress[$i]?></td>
			<td><?php echo $userphone[$i]?></td>
			<td><?php echo $useremail[$i]?></td>
			<td style="text-align:center;"><?php echo '<button class="btn btn-success editbtn" contenteditable="false">Edit</button> ';[$i]?></td>
			<td style="text-align:center;"><?php echo '<button class="btn btn-success delbtn" contenteditable="false">Delete</button> ';[$i]?></td>
	      </tr>
		  <?php }} ?>	 
</tbody>
		</table>
				</div>
			</div>
			</div>
	
		<div class="col-md-1">&nbsp;</div>
		
	<div class="row">
		
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
			<script>
				$(document).ready(function(){
					$('.editbtn').on('click', function(){
						$('#editmodal').modal('show');

							$tr = $(this).closest('tr');

							var data =$tr.children("td").map(function(){
								return $(this).text();
							}).get();
							console.log(data);

                        $('#usersid').val(data[0]);
                        $('#firstname').val(data[1]);
                        $('#lastname').val(data[2]);
                        $('#dateregistered').val(data[3]);
                        $('#address').val(data[4]);
                        $('#phonenum').val(data[5]);
                        $('#email').val(data[6]);
                       
                       
							
					});

				});
			</script>
			<script>
				$(document).ready(function(){
					$('.delbtn').on('click', function(){
						$('#deletemodal').modal('show');

							$tr = $(this).closest('tr');

							var data =$tr.children("td").map(function(){
								return $(this).text();
							}).get();
							console.log(data);

                        $('#usersid').val(data[0]);
                        $('#firstname').val(data[1]);
                        $('#lastname').val(data[2]);
                        $('#dateregistered').val(data[3]);
                        $('#address').val(data[4]);
                        $('#phonenum').val(data[5]);
                        $('#email').val(data[6]);
                       
                       
							
					});

				});
			</script>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
			   
			  
<table class="table table-striped">
		<div class="container">
		<div class="row">
		<div class="col-md-12">
		</div>
        </div>
        </div>
        </div>
  </body>
</html>
